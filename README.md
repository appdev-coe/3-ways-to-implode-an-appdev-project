# [3 Ways to Implode an AppDev Project](https://appdev-coe.gitlab.io/3-ways-to-implode-an-appdev-project)

 - [Lightning talk session for 3 Ways to Implode an AppDev Project](https://appdev-coe.gitlab.io/3-ways-to-implode-an-appdev-project/lightning-talk.html)

 - [Full session for 3 Ways to Implode an AppDev Project](https://appdev-coe.gitlab.io/3-ways-to-implode-an-appdev-project/full-talk.html)


## Abstract:
Development projects are challenging for many reasons. It’s hard enough to guide one smoothly to completion, but how can you survive while others around you lose their minds? In this session we’ll provide three insights on how to remain calm, assist frustrated colleagues, and improves your team’s mental health. Attendees are provided with these insights that are based on years of open source application development teams (developers, designers, and project managers) surviving daily in the trenches. They can take these home and apply them effectively to their own projects.

 - Lying to yourself and you team (clear communication is important)

 - Deafness is the silent killer (not listening kills, can’t hear the train coming)

 - Stealing their souls (time, efficiency, and chaos)

[![Cover Slide](https://gitlab.com/appdev-coe/3-ways-to-implode-an-appdev-project/raw/master/cover.png)](https://appdev-coe.gitlab.io/3-ways-to-implode-an-appdev-project)